# Задача 1. Количество слов
# https://dmoj.ca/problem/dmopc15c7p2
# Ввод str строка не более 80 символов
# Вывод total цифра кол-во слов в вводимой строке

str1 = input();


def get_total_words(str):
    return str.count(' ') + 1


print(get_total_words(str1))
