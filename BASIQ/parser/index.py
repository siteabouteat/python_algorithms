from bs4 import BeautifulSoup
import urllib.request

req = urllib.request.urlopen('https://calory-hunter.ru/')
html = req.read()

soup = BeautifulSoup(html, 'html.parser')

blog_title = soup.findAll('div', class_='header-slogan')

print(f'blog title: {blog_title}')
