# Неизменяемый список (2, 101, 77, 11, 0, 1)
# tuple(1, 2, ['hello', 'world'], 0)

# В элементы кортежа можно добавлять новые элементы
t1 = (1, 2, ['hello', 'world'], 0)

print(f't1={t1}')

t1[2].append('!')

print(f't1 after add new el "!": {t1}')

# Распаковка кортежа

x, y, z = (1, 2, True)

print(f'x: {x} - y: {y} - z: {z}')

x = 1
y = 2

print(f'before: x:{x}:y:{y}')

x, y = y, x

print(f'after: x:{x}:y:{y}')

