import re

str1 = 'problem one is really lOng'


def find_cnt_words_in_string(str):
    return str.count(' ') + 1


print(find_cnt_words_in_string(str1))

str2 = 'Should count all vowels'


def get_count(sentence):
    pattern = r'[aeiou]'
    return len(re.findall(pattern, sentence))


print(f'count: {get_count(str2)}')


def disemvowel(sentence):
    pattern = r'[aeiou]'
    return re.sub(pattern, '', sentence, flags=re.I)


print(f'count: {disemvowel(str2)}')
