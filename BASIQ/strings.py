words = ['мадам', 'топот', 'test', 'madam', 'word']
word = 'Hello world my friends!!!'


def replace_spaces(str):
    return str.replace(' ', '')


print(f'replace spaces: {replace_spaces(word)}')


def palindrome(arr):
    res = []
    for word in arr:
        if word == word[::-1]:
            res.append(word)
    return res


print(f'palindrome: {palindrome(words)}')
