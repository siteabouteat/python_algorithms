list1 = [1, 2, 3]
list2 = [i * 2 for i in list1]
PI = 3.141592653589793
print(list2)

hw_str = "Hello world!"

print(f'this is format string: {hw_str}')


# тернарный оператор
def even_or_odd(n):
    return 'Even' if n % 2 == 0 else 'Odd'


def spooky_season():
    n = int(input())
    return 'sp' + 'o' * n + 'ky'


print(f'spooky: {spooky_season()}')


# Вычислить объем куба формула: (πr2h)/3
def core_drill():
    r = int(float(input()))
    h = int(float(input()))
    return (PI * r ** 2 * h) / 3


print(f'core drill: {core_drill()}')


def find_space_in_string(str):
    if " " in str:
        return True
    else:
        return False


print(find_space_in_string(hw_str))


def get_sum_in_list_values(list_vals):
    result = 0
    for n in list_vals:
        result += n
    return result


print(get_sum_in_list_values(list1))


def dincrease_the_number_by_three(list_vals):
    result = []
    for i in list_vals:
        result.append(i * 3)
    return result


print(dincrease_the_number_by_three(list1))
