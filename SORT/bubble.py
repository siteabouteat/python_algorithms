notSortArr = [99, 4, 1, 2, 5, 3, 6, 7]
sortArr = [1, 2, 3, 4, 5, 6, 7, 8, 9]


def bubble_sort(arr):
    for j in range(len(arr) - 1):
        no_swap = True
        for i in range(len(arr) - 1 - j):
            if arr[i] > arr[i + 1]:
                arr[i], arr[i + 1] = arr[i + 1], arr[i]  # правую часть присваиваю в левую для массива
                no_swap = False
        if no_swap:
            return arr
    return arr


print(bubble_sort(notSortArr))
