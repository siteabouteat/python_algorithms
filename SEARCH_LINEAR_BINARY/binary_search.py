sorted_list = [1, 2, 3, 55, 77, 99, 101]


def bs(nsort, target):
    start = 0
    end = len(nsort) - 1
    return False


# print(bs(sorted_list, 99))


def binary_search(sort_list, target):
    start = 0
    end = len(sort_list) - 1
    while end >= start:
        mid = (start + end) // 2
        if sort_list[mid] == target:
            return {'value': target, 'index': mid}
        if sort_list[mid] < target:
            start = mid + 1
        if sort_list[mid] > target:
            end = mid - 1
    return False


# print(binary_search(sorted_list, 99))
