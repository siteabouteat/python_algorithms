class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BinarySearchTree:
    def __init__(self):
        self.root = None

    def __find(self, current_node, parent_node, value):
        if current_node is None:
            return None, parent_node, False

        if value == current_node.data:
            return current_node, parent_node, True

        if value < current_node.data:
            if current_node.left:
                return self.__find(current_node.left, current_node, value)

        if value > current_node.data:
            if current_node.right:
                return self.__find(current_node.right, current_node, value)

        return current_node, parent_node, False

    def append(self, obj):
        if self.root is None:
            self.root = obj
            return obj

        current_node, parent_node, flag = self.__find(self.root, None, obj.data)

        if not flag and current_node:
            if obj.data < current_node.data:
                current_node.left = obj
            else:
                current_node.right = obj
        return obj

    def show_tree(self, node):
        if node is None:
            return
        self.show_tree(node.left)
        print(node.data)
        self.show_tree(node.right)

    def __del_leaf_without_children(self, current_node, parent_node):
        if parent_node.left == current_node:
            parent_node.left = None
        elif parent_node.right == current_node:
            parent_node.right = None

    def __del_with_one_child(self, current_node, parent_node):
        # К какой вершине(лево\право) родителя подцеплена удаляемая нода
        if parent_node.left == current_node:
            if current_node.left is None:
                parent_node.left = current_node.right
            elif current_node.right is None:
                parent_node.left = current_node.left
        if parent_node.right == current_node:
            if current_node.left is None:
                parent_node.right = current_node.right
            elif current_node.right is None:
                parent_node.right = current_node.left


    def __del_with_two_child(self, current_node, parent_node):


    def delete_node(self, key):
        current_node, parent_node, flag = self.__find(self.root, None, key)

        if not flag:
            return None

        if current_node.left is None and current_node.left is None:
            self.__del_leaf_without_children(current_node, parent_node)
        elif current_node.left is None or current_node.right is None:
            self.__del_with_one_child(current_node, parent_node)
        elif current_node.left is not None and current_node.right is not None:
            self.__del_with_two_child(current_node, parent_node)

list_values = [10, 5, 7, 16, 13, 2, 20]

bst = BinarySearchTree()

for item in list_values:
    bst.append(Node(item))

bst.show_tree(bst.root)
