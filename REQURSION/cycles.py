def printNums(n, rej):
    if n > rej:
        return
    print(n)
    return printNums(n+1, 10)

printNums(1, 10)

# [
#     printNums(1, 10) #
#     printNums(2, 10) #
#     printNums(3, 10) #
#     printNums(4, 10) #
#     printNums(5, 10) #
#     printNums(6, 10) #
#     printNums(7, 10) #
#     printNums(8, 10) #
#     printNums(9, 10) #
#     printNums(10, 10) #
#     printNums(11, 10) # null
# ]